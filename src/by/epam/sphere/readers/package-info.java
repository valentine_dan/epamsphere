/**
 * This package contains classes to read information from file.
 * Class FileReader helps to read Strings from file and
 * returns list of read Strings
 */
package by.epam.sphere.readers;
