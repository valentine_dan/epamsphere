package by.epam.sphere.readers;

import by.epam.sphere.exceptions.FileException;
import by.epam.sphere.validators.ReadingValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for reading Strings from file.
 *
 * @author Valentin Danilov
 */
public class FileReader {

    private static final Logger LOG = LogManager.getRootLogger();

    /**
     * This method reads Strings from file.
     *
     * @param file file to be read
     * @return list of read Srings
     */
    public List<String> readFile(final File file) {
        List<String> resultList = new ArrayList<>();
        if (ReadingValidator.isValid(file)) {
            try (Stream<String> lineStream = Files.
                    lines(Paths.get(file.getPath()))) {
                resultList = lineStream.skip(1).collect(Collectors.toList());
            } catch (IOException ex) {
                LOG.error("File " + file.getName()
                        + " wasn't closed correctly");
                LOG.error(ex.getStackTrace());
            }
        } else {
            throw new FileException("Something wrong with file!");
        }
        return resultList;
    }
}
