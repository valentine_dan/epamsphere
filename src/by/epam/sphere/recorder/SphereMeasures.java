package by.epam.sphere.recorder;

import by.epam.sphere.actions.SphereAction;
import by.epam.sphere.observer.Observer;

/**
 * Class-recorder that will store
 * volume and surface area for corresponding sphere.
 * Implements Observer.
 */
public class SphereMeasures implements Observer {
    /**
     * Sphere's volume.
     */
    private double sphereVolume;
    /**
     * Sphere's surface area.
     */
    private double sphereSurfaceArea;


    /**
     * Getter fot volume.
     *
     * @return current volume
     */
    public double getSphereVolume() {
        return sphereVolume;
    }

    /**
     * Setter for volume.
     *
     * @param sphereVolumeNew - new volume
     */
    public void setSphereVolume(final double sphereVolumeNew) {
        this.sphereVolume = sphereVolumeNew;
    }

    /**
     * Getter for surface area.
     *
     * @return current surface area
     */
    public double getSphereSurfaceArea() {
        return sphereSurfaceArea;
    }

    /**
     * Setter for surface area.
     *
     * @param sphereSurfaceAreaNew - new surface area
     */
    public void setSphereSurfaceArea(final double sphereSurfaceAreaNew) {
        this.sphereSurfaceArea = sphereSurfaceAreaNew;
    }

    /**
     * String representation for SphereMeasures class.
     *
     * @return string representation of SphereMeasures class
     */
    @Override
    public String toString() {
        return "SphereMeasures{".
                concat("sphereVolume=").
                concat(String.format("%.3f", sphereVolume)).
                concat("; sphereSurfaceArea=").
                concat(String.format("%.3f", sphereSurfaceArea)).
                concat("}");
    }

    /**
     * Updates current measures.
     *
     * @param newRadius - new radius
     */
    @Override
    public void update(final double newRadius) {
        sphereVolume = SphereAction.calcVolume(newRadius);
        sphereSurfaceArea = SphereAction.calcSurfaceArea(newRadius);
    }
}

