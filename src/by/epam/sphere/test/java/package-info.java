/**
 * Package contains classes for testing logic of project classes
 *
 * SphereActionTest - class contains test for SphereAction class
 *
 * Reader test - class contains test fot FileReader class
 *
 * ValidatorsTest - class contains tests for ReadingValidator
 * and StringDataValidator classes
 */
package by.epam.sphere.test.java;
