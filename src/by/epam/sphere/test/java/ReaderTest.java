package by.epam.sphere.test.java;

import by.epam.sphere.exceptions.FileException;
import by.epam.sphere.readers.FileReader;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Testing class for testing work of file reader.
 */
public class ReaderTest {
    /**
     * Instance of FileReader to test.
     */
    private FileReader fileReader = new FileReader();

    /**
     * Path to file.
     */
    private static final String PATH_EMPTY = "data/empty.txt";

    /**
     * Test fot readFile() method.
     */

    @Test(expectedExceptions = FileException.class, suiteName = "Basic tests",testName = "reading")
    public void testFileIsEmpty() {
        fileReader.readFile(new File(PATH_EMPTY));
    }
}

