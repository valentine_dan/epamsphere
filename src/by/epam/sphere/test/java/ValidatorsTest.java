package by.epam.sphere.test.java;

import by.epam.sphere.validators.DataValidator;
import by.epam.sphere.validators.StringDataValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Class for testing methods for validating data.
 *
 * @author Valentin Danilov
 */
public class ValidatorsTest {
    /**
     * Method for providing parameters to test.
     *
     * @return array of testParsedData() method
     */
    @DataProvider
    public Object[][] dataForTestStrongValidator() {
        return new Object[][]{
                {List.of("2", "2", "2", "2"), true},
                {List.of("1.000", "0002.1", "-0001.3", "23"), true},
                {List.of("1", "1.2", "1.3"), false},
                {List.of("-0.23"), false},
                {List.of("1.312", "2.34", "211.123"), false},
                {List.of("12x", "23", "21.3", "12"), false}
        };
    }

    /**
     * Method for providing parameters to test.
     *
     * @return array of testParsedData() method
     */
    @DataProvider
    public Object[][] dataForTestDataValidator() {
        return new Object[][]{
                {List.of(2.0, 1.123, 0.0, 0.0), false},
                {List.of(1.23, 4.123, 0.12, -231.0), false},
                {List.of(1.2, 1.3, 1.4, 12.12), true}
        };
    }

    /**
     * Test for StringDataValidator.isValid() method.
     *
     * @param data     data to validate
     * @param expected expected result
     */
    @Test(dataProvider = "dataForTestStrongValidator", suiteName = "Basic tests", testName = "validating")
    public void testStringValidator(final List<String> data,
                                    final Boolean expected) {
        Boolean actual = StringDataValidator.isValid(data);
        Assert.assertEquals(actual, expected);
    }

    /**
     * Test for DataValidator.isValid() method.
     *
     * @param data     data to validate
     * @param expected expected result
     */
    @Test(dataProvider = "dataForTestDataValidator", suiteName = "Basic tests", testName = "validating")
    public void testDataValidator(final List<Object> data,
                                  final boolean expected) {
        boolean actual = DataValidator.isValid(data);
        Assert.assertEquals(actual, expected);
    }

}
