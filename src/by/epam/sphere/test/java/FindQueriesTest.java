package by.epam.sphere.test.java;

import by.epam.sphere.creators.SphereCreator;
import by.epam.sphere.entities.Point;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.repository.SphereRepository;
import by.epam.sphere.repository.queryspecification.findqueries.*;
import by.epam.sphere.storages.SphereStorage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class for testing Find Queries.
 */
public class FindQueriesTest {
    /**
     * Repository for testing.
     */
    private SphereRepository sr =
            new SphereRepository(SphereStorage.getInstance());
    /**
     * Creator for Sphere class instances.
     */
    private SphereCreator creator = new SphereCreator();

    /**
     * Method for initiating repository.
     */
    @BeforeClass
    public void init() {
        sr.add(creator.createFigure(List.of("one", 0.0, 1.1, 2.2, 3.3)));
        sr.add(creator.createFigure(List.of("one", 0.1, 0.0, 2.2, 4.0)));
        sr.add(creator.createFigure(List.of("two", 0.2, 0.2, 0.2, 3.0)));
        sr.add(creator.createFigure(List.of("three", 0.2, 0.2, 0.2, 4.0)));
        sr.add(creator.createFigure(List.of("four", 2.2, -12.0, -23.0, 23.0)));
        sr.add(creator.createFigure(List.of("five", -2.2, -12.0, -23.0, 1.45)));
        sr.add(creator.createFigure(List.of("six", 0.0, 1.1, 2.2, 3.3)));
        sr.add(creator.createFigure(List.of("six", 4.0, 4.0, 4.0, 4.0)));
    }


    /**
     * Method that provides data for
     * testFindQueries() method.
     *
     * Data pattern: (Query, Result List of Spheres)
     *
     * @return array of parameters for
     * testFindQueries() method
     */
    @DataProvider
    public Object[][] findQueriesData() {
        return new Object[][]{
                {
                        new FindByCenterQuery(new Point(0.2, 0.2, 0.2)),
                        List.of(creator.createFigure(
                                List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("three", 0.2, 0.2, 0.2, 4.0)))
                },
                {
                        new FindByCenterQuery(new Point(0.0, 0.0, 0.0)),
                        List.of()
                },
                {
                        new FindByDistanceFromAxesOriginQuery(0.0, 5.0),
                        List.of(creator.createFigure(
                                List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("one", 0.1, 0.0, 2.2, 4.0)),
                                creator.createFigure(
                                        List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("three", 0.2, 0.2, 0.2, 4.0)),
                                creator.createFigure(
                                        List.of("six", 0.0, 1.1, 2.2, 3.3)))
                },
                {
                        new FindByDistanceFromAxesOriginQuery(100.0, 200.0),
                        List.of()},
                {new FindByIdQuery("1one"),
                        List.of(creator.createFigure(
                                List.of("one", 0.0, 1.1, 2.2, 3.3)))
                },
                {
                        new FindByIdQuery("123one"),
                        List.of()
                },
                {
                        new FindByNameQuery("six"),
                        List.of(creator.createFigure(
                                List.of("six", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("six", 4.0, 4.0, 4.0, 4.0)))
                },
                {
                        new FindByNameQuery("seven"),
                        List.of()
                },
                {
                        new FindByRadiusQuery(4.0),
                        List.of(creator.createFigure(
                                List.of("one", 0.1, 0.0, 2.2, 4.0)),
                                creator.createFigure(
                                        List.of("three", 0.2, 0.2, 0.2, 4.0)),
                                creator.createFigure(
                                        List.of("six", 4.0, 4.0, 4.0, 4.0)))
                },
                {
                        new FindByRadiusQuery(5.0),
                        List.of()
                },
                {
                        new FindBySurfaceAreaQuery(12.0, 114.0),
                        List.of(creator.createFigure(
                                List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("five", -2.2, -12.0, -23.0, 1.45)))
                },
                {
                        new FindBySurfaceAreaQuery(1.0, 2.0),
                        List.of()
                },
                {
                        new FindByVolumeQuery(124.0, 269.0),
                        List.of(creator.createFigure(
                                List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("one", 0.1, 0.0, 2.2, 4.0)),
                                creator.createFigure(
                                        List.of("three", 0.2, 0.2, 0.2, 4.0)),
                                creator.createFigure(
                                        List.of("six", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("six", 4.0, 4.0, 4.0, 4.0)))
                },
                {
                        new FindByVolumeQuery(1.0, 2.0),
                        List.of()
                }
        };
    }

    /**
     * Method for testing Find queries.
     *
     * @param findQuery - corresponding query
     * @param expected  - expected list
     */
    @Test(dataProvider = "findQueriesData")
    public void testFindQueries(final FindQuery findQuery,
                                final List<Sphere> expected) {
        List<Sphere> actual = sr.query(findQuery);
        List<Sphere> ex = new ArrayList<>(expected);
        ex.sort(Comparator.comparing(Sphere::getName));
        actual.sort(Comparator.comparing(Sphere::getName));
        System.out.println(actual);
        System.out.println(ex);
        Assert.assertEquals(ex, actual);
    }
}

