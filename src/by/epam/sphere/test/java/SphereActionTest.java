package by.epam.sphere.test.java;

import by.epam.sphere.actions.SphereAction;
import by.epam.sphere.creators.SphereCreator;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.enumerations.CoordPlanes;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Class for testing methods of SphereAction class.
 *
 * @author Valentin Danilov
 */
public class SphereActionTest {
    /**
     * Sphere creator.
     */
    private SphereCreator creator = new SphereCreator();

    /**
     * Parameters pattern:
     * (Sphere sphere, expected surface area, expected volume area, eps).
     *
     * @return array of test's parameters
     */
    @DataProvider
    public Object[][] sphereMeasuresData() {
        return new Object[][]{
                {creator.createFigure(List.of("name", 1.0, 1.0, 1.0, 25.0)),
                        7853.981633, 65449.846949, 10e-7},
                {creator.createFigure(List.of("name", -1.0, 1.0, 1.123, 1.234e25)),
                        1.913551e51, 7.871075e75, 10e68},
                {creator.createFigure(List.of("name", 23.0, 23.1, 23.2, 5.678e-25)),
                        4.051358e-48, 7.667870e-73, 10e-7},
        };
    }

    /**
     * Parameters pattern: (Sphere sphere, expected value).
     *
     * @return array of test's parameters
     */
    @DataProvider
    public Object[][] doesTouchCoordPlanesData() {

        return new Object[][]{
                {creator.createFigure(List.of("name", 3.0, -3.0, 2.0, 2.0)),
                        true},
                {creator.createFigure(List.of("name", -25.0, -3234.0, 2.1, 2.005)),
                        false},
                {creator.createFigure(List.of("name", 1.1234, 1.1234, -1.0, 1.12343)),
                        false},
                {creator.createFigure(List.of("name", 1.123123, -0.2354, 2.0, 0.2354)),
                        true}
        };
    }

    /**
     * Parameters pattern: (Sphere sphere, height, expected value, eps).
     *
     * @return array of test's parameters
     */
    @DataProvider
    public Object[][] segmentVolumeData() {
        return new Object[][]{
                {creator.createFigure(List.of("name", 3.0, -3.0, 2.0, 2.0)),
                        1.5, 12.5664, 10e-4},
                {creator.createFigure(List.of("name", 3.0, -3.0, 2.0, 12.2134)),
                        5.32, 94320.0930, 10e-4},
        };
    }

    /**
     * Method that providing parameters for
     * test of findRatio() method
     *
     * @return - array of parameters
     */
    @DataProvider
    public Object[][] findRatioData() {
        return new Object[][]{
                {creator.createFigure(List.of("name", 2.0, 2.0, 2.0, 2.0)),
                        2.0, CoordPlanes.XY, 1.0, 0.0},
                {creator.createFigure(List.of("name", 0.0, 0.0, 0.0, 21.12)),
                        21.13, CoordPlanes.YZ, -1.0, 0.0},
                {creator.createFigure(List.of("name", -1.0, 1.0, 1.0, 30.0)),
                        -29.0, CoordPlanes.YZ, 0.0333, 10e-4},
                {creator.createFigure(List.of("name", 5.0, 4.0, -3.0, 12.0)),
                        16.0, CoordPlanes.XZ, 0.0, 0.0}
        };
    }


    /**
     * Test for calcVolume() method.
     *
     * @param sphere       sphere to test
     * @param expectedArea expected area for corresponding sphere
     * @param expectedVol  expected volume for corresponding sphere
     * @param eps          accuracy
     */
    @Test(dataProvider = "sphereMeasuresData", suiteName = "Basic tests", testName = "action")
    public void testCalcVolume(final Sphere sphere,
                               final Double expectedArea,
                               final Double expectedVol,
                               final Double eps) {
        double actualVol = SphereAction.calcVolume(sphere.getRadius());
        Assert.assertTrue(actualVol - expectedVol <= eps);
    }

    /**
     * Test for calcSurfaceArea() method.
     *
     * @param sphere       sphere to test
     * @param expectedArea expected area for corresponding sphere
     * @param expectedVol  expected volume for corresponding sphere
     * @param eps          accuracy
     */
    @Test(dataProvider = "sphereMeasuresData", suiteName = "Basic tests", testName = "action")
    public void testCalcSurfaceArea(final Sphere sphere,
                                    final Double expectedArea,
                                    final Double expectedVol,
                                    final Double eps) {
        double actualArea = SphereAction.calcSurfaceArea(sphere.getRadius());
        Assert.assertTrue(actualArea - expectedArea <= eps);
    }

    /**
     * Test for doesTouchCoordPlanes() method.
     *
     * @param sphere   sphere to test
     * @param expected expected value
     */
    @Test(dataProvider = "doesTouchCoordPlanesData")
    public void testDoesTouchCoordPlanes(final Sphere sphere,
                                         final boolean expected){
        SphereAction action = new SphereAction(sphere);
        boolean actual = action.doesTouchCoordPlane();
        Assert.assertEquals(actual, expected);
    }


    /**
     * Test for calcSegmentVol() method.
     *
     * @param sphere   sphere to test
     * @param height   height of segment
     * @param expected expected volume of corresponding segment
     * @param eps      accuracy
     */
    @Test(dataProvider = "segmentVolumeData", suiteName = "Basic tests", testName = "action")
    public void testSegmentVol(final Sphere sphere,
                               final Double height,
                               final Double expected,
                               final Double eps) {
        SphereAction action = new SphereAction(sphere);
        double actualVol = action.calcSegmentVolume(height);
        Assert.assertTrue(actualVol - expected <= eps);
    }

    /**
     * Test for findRatio() method.
     *
     * @param sphere     - Sphere object
     * @param planeCoord - coordinate of intersection
     *                   of dividing plane
     *                   and corresponding coordinate axis
     * @param coordPlane - coordinate plane parallel
     *                   to dividing plane
     * @param expected   - expected ratio
     * @param eps        - accuracy
     */
    @Test(dataProvider = "findRatioData", suiteName = "Basic tests", testName = "action")
    public void testFindRatio(final Sphere sphere,
                              final Double planeCoord,
                              final CoordPlanes coordPlane,
                              final Double expected,
                              final Double eps) {
        SphereAction action = new SphereAction(sphere);
        double actual = action.findVolumeRatio(planeCoord, coordPlane);
        Assert.assertTrue(actual - expected <= eps);
    }
}


