package by.epam.sphere.test.java;

import by.epam.sphere.creators.SphereCreator;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.repository.SphereRepository;
import by.epam.sphere.repository.queryspecification.sortqueries.*;
import by.epam.sphere.storages.SphereStorage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Class for testing Sort queries.
 */
public class SortQueriesTest {
    /**
     * Test repository.
     */
    private SphereRepository sr =
            new SphereRepository(SphereStorage.getInstance());
    /**
     * Creator for Sphere instances.
     */
    private SphereCreator creator = new SphereCreator();

    /**
     * Method for initiating repository.
     */
    @BeforeClass
    public void init() {
        sr.add(creator.createFigure(List.of("one", 0.0, 1.1, 2.2, 3.3)));
        sr.add(creator.createFigure(List.of("two", 0.2, 0.2, 0.2, 3.0)));
        sr.add(creator.createFigure(List.of("three", -23.1, -0.35, 0.1, 4.0)));
    }

    /**
     * Method that provides data for
     * testSortQueries() method.
     *
     * Data pattern: (Query, Result List of Spheres)
     *
     * @return array of parameters for
     * testSortQueries() method
     */
    @DataProvider
    public Object[][] sortQueriesData() {
        return new Object[][]{
                {
                        new SortByNameQuery(),
                        List.of(creator.createFigure(
                                List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("three", -23.1, -0.35, 0.1, 4.0)),
                                creator.createFigure(
                                        List.of("two", 0.2, 0.2, 0.2, 3.0)))
                },
                {
                        new SortByRadiusQuery(),
                        List.of(creator.createFigure(
                                List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("three", -23.1, -0.35, 0.1, 4.0)))
                },
                {
                        new SortBySurfaceAreaQuery(),
                        List.of(creator.createFigure(
                                List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("three", -23.1, -0.35, 0.1, 4.0)))
                },
                {
                        new SortByVolumeQuery(),
                        List.of(creator.createFigure(
                                List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("three", -23.1, -0.35, 0.1, 4.0)))
                },
                {
                        new SortByXCoordinateQuery(),
                        List.of(creator.createFigure(
                                List.of("three", -23.1, -0.35, 0.1, 4.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)),
                                creator.createFigure(
                                        List.of("two", 0.2, 0.2, 0.2, 3.0)))
                },
                {
                        new SortByYCoordinateQuery(),
                        List.of(creator.createFigure(
                                List.of("three", -23.1, -0.35, 0.1, 4.0)),
                                creator.createFigure(
                                        List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)))},
                {
                        new SortByZCoordinateQuery(),
                        List.of(creator.createFigure(
                                List.of("three", -23.1, -0.35, 0.1, 4.0)),
                                creator.createFigure(
                                        List.of("two", 0.2, 0.2, 0.2, 3.0)),
                                creator.createFigure(
                                        List.of("one", 0.0, 1.1, 2.2, 3.3)))}
        };
    }

    /**
     * Method for testing Sort queries.
     *
     * @param sortQuery - corresponding query
     * @param expected  - expected list
     */
    @Test(dataProvider = "sortQueriesData")
    public void testSortQueries(final SortQuery sortQuery,
                                final List<Sphere> expected) {
        List<Sphere> actual = sr.query(sortQuery);
        Assert.assertEquals(expected, actual);
    }
}
