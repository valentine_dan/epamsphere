package by.epam.sphere.entities;

/**
 * Entity class for point with x,y,z coordinates.
 *
 * @author Valentin Danilov
 */
public class Point implements GeometricFigure {
    /**
     * x coordinate of point.
     */
    private double x;
    /**
     * y coordinate of point.
     */
    private double y;
    /**
     * z coordinate of point.
     */
    private double z;

    /**
     * Default constructor.
     */
    public Point() {
    }

    /**
     * @param newX - x coord
     * @param newY - y coord
     * @param newZ - z coord
     */
    public Point(final double newX, final double newY, final double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    /**
     * @return x coord
     */
    public double getX() {
        return x;
    }

    /**
     * Sets new value to x coord.
     *
     * @param newX - new value
     */
    public void setX(final double newX) {
        this.x = newX;
    }

    /**
     * @return y coord
     */
    public double getY() {
        return y;
    }

    /**
     * Sets new value to y coord.
     *
     * @param newY - new y coord
     */
    public void setY(final double newY) {
        this.y = newY;
    }

    /**
     * @return z coord
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets new value to z coord.
     *
     * @param newZ - new z coord
     */
    public void setZ(final double newZ) {
        this.z = newZ;
    }

    /**
     * Overrided method to compare two objects.
     *
     * @param obj - object to compare with this
     * @return true if objects are equals, else returns false
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Point)) {
            return false;
        }
        Point point = (Point) obj;
        return Double.compare(point.x, x) == 0
                &&
                Double.compare(point.y, y) == 0
                &&
                Double.compare(point.z, z) == 0;
    }

    /**
     * Overrided method to calculate hash code for current object.
     *
     * @return calculated hash code
     */
    @Override
    public int hashCode() {
        int magicNumber1 = 17;
        int magicNumber2 = 31;
        int result = magicNumber1;
        result = result * magicNumber2 + Double.hashCode(x);
        result = result * magicNumber2 + Double.hashCode(y);
        result = result * magicNumber2 + Double.hashCode(z);
        return result;
    }

    /**
     * Overrided method for string representation
     * of current object.
     *
     * @return string representation of current object
     */
    @Override
    public String toString() {
        return "Point{".
                concat("x=").
                concat(Double.toString(x)).
                concat(", y=").
                concat(Double.toString(y)).
                concat(", z=").
                concat(Double.toString(z)).
                concat("}");
    }
}

