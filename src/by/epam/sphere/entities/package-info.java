/**
 * Package for entity classes.
 *
 * GeometricFigure - interface-marker for geometric figures
 *
 * Point - entity class for representation of Point
 *
 * Sphere - entity class for representation of Sphere
 */
package by.epam.sphere.entities;
