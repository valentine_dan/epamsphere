package by.epam.sphere.entities;

/**
 * Interface marker for class that represents
 * geometric figures.
 * @author Valentin Danilov
 */
public interface GeometricFigure {
}
