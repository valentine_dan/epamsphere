package by.epam.sphere.entities;

import by.epam.sphere.observer.Observable;
import by.epam.sphere.recorder.SphereMeasures;


/**
 * Entity-class for Sphere.
 *
 * @author Valentin Danilov
 */
public class Sphere implements GeometricFigure, Observable {
    /**
     * Center of Sphere.
     */
    private Point center;
    /**
     * Radius of Sphere.
     */
    private double radius;
    private String name;
    private SphereMeasures measures;

    /**
     * Default constructor.
     */
    public Sphere() {
    }

    /**
     * @param newCenter - center of Sphere
     * @param newRadius - radius of Sphere
     */
    public Sphere(final Point newCenter, final double newRadius) {
        this.center = newCenter;
        this.radius = newRadius;
        notify(newRadius);
    }

    public Sphere(String name, Point center, double radius) {
        measures = new SphereMeasures();
        this.center = center;
        this.radius = radius;
        this.name = name;
        notify(radius);
    }

    /**
     * @return center of current Sphere
     */
    public Point getCenter() {
        return center;
    }

    /**
     * Sets new value for center.
     *
     * @param newCenter - new value for center
     */
    public void setCenter(final Point newCenter) {
        this.center = newCenter;
    }

    /**
     * @return radius of current Sphere
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Sets new value for radius.
     *
     * @param newRadius - new value for radius
     */
    public void setRadius(final double newRadius) {
        this.radius = newRadius;
        notify(newRadius);
    }

    public void setMeasures(final SphereMeasures measures) {
        this.measures = measures;
    }

    public SphereMeasures getMeasures() {
        return measures;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Overrided method to compare two objects.
     *
     * @param obj - object to compare with this
     * @return true if objects are equals, else returns false
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Sphere)) {
            return false;
        }
        Sphere sphere = (Sphere) obj;
        return Double.compare(sphere.radius, radius) == 0
                && center.equals(sphere.getCenter());
    }

    /**
     * Overrided method to calculate hash code for current object.
     *
     * @return calculated hash code
     */
    @Override
    public int hashCode() {
        int magicNumber1 = 17;
        int magicNumber2 = 31;
        int result = magicNumber1;
        result = result * magicNumber2 + Double.hashCode(radius);
        result = result * magicNumber2 + center.hashCode();
        return result;
    }

    /**
     * Overrided method for string representation
     * of current object.
     *
     * @return string representation of current object
     */
    @Override
    public String toString() {
        return "Sphere{".
                concat("name=").
                concat(name).
                concat(", center=").
                concat(center.toString()).
                concat(", radius=").
                concat(Double.toString(radius)).
                concat("}");
    }

    @Override
    public void notify(final double newRad) {
        measures.update(newRad);
    }
}

