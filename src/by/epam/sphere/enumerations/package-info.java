/**
 * Package which contains enums
 *
 * CoordPlanes - enum for coordinate plane in Cartesian coordinate system
 */
package by.epam.sphere.enumerations;
