package by.epam.sphere.enumerations;

/**
 * Enum which helps to identify
 * coordinate plane.
 *
 * @author Valentin Danilov
 */
public enum CoordPlanes {
    /**
     * XY coordinate plane.
     */
    XY,
    /**
     * XZ coordinate plane.
     */
    XZ,
    /**
     * YZ coordanate plane.
     */
    YZ
}
