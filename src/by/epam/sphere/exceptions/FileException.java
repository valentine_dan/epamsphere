package by.epam.sphere.exceptions;

/**
 * Class of custom exception.
 * @author Valentin Danilov
 */
public class FileException extends RuntimeException {
    /**
     * Constructor for throwing an exception.
     * @param message - message
     */
    public FileException(String message){
        super(message);
    }
}
