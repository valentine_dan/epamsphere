/**
 * Package that contains classes of custom
 * exceptions
 *
 * FileException - custom exception class for issues emerging with files
 */
package by.epam.sphere.exceptions;
