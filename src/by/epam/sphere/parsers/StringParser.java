package by.epam.sphere.parsers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for parsing input String into tokens.
 *
 * @author Valentin Danilov
 */
public class StringParser {
    /**
     * Method takes String as a parameter and
     * returns list of it's tokens.
     *
     * @param data - string to parse
     * @return list of tokens
     */
    public List<String> parseString(final String data) {
        return Stream.of(data.split(" ")).map(String::new).
                collect(Collectors.toList());
    }
}

