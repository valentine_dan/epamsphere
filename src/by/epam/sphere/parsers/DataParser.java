package by.epam.sphere.parsers;

import java.util.List;

/**
 * Class that parses input string into double.
 *
 * @author Valentin Danilov
 */
public class DataParser {
    /**
     * Method that parses input string into double.
     * @param data - input string
     * @return - list of double parameters
     */
    public List<Object> parseData(final String data) {
        StringParser stringParser = new StringParser();
        DoubleParser doubleParser = new DoubleParser();
        List<String> stringData = stringParser.parseString(data);
        return doubleParser.parseToDouble(stringData);
    }
}

