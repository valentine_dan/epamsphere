package by.epam.sphere.parsers;

import by.epam.sphere.validators.StringDataValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that parses list of strings into list of doubles.
 */
public class DoubleParser {
    /**
     * Method that parses list of string into list of doubles.
     *
     * @param data - list of strings to parse
     * @return - list of doubles if data is valid,
     * else empty list
     */
    public List<Object> parseToDouble(final List<String> data) {
        if (StringDataValidator.isValid(data)) {
            final int nameIndex = 0;
            List<Object> parsed = new ArrayList<>();
            parsed.add(data.get(nameIndex));
            parsed.addAll(data.stream().skip(1).
                    map(Double::parseDouble).
                    collect(Collectors.toList()));
            return parsed;
        } else {
            return new ArrayList<>();
        }
    }
}
