/**
 * Package that contains classes for parsing read data.
 *
 * DataParser - class that parse input string to double
 * Uses methods of StringParser and DoubleParser classes
 *
 * DoubleParser - class that parses input list of string parameters to double
 *
 * StringParser - class that parses input string into list of string parameters
 */
package by.epam.sphere.parsers;
