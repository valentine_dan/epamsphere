package by.epam.sphere.storages;

/**
 * Class that generates ID.
 */
public final class IDGenerator {
    /**
     * current ID.
     */
    private static Integer id = 1;

    /**
     * private constructor.
     */
    private IDGenerator() {
    }

    /**
     * @return id
     */
    public static Integer getId() {
        return id;
    }

    /**
     * Generates next ID.
     *
     * @return next id
     */
    public static Integer generateNextId() {
        return id++;
    }
}
