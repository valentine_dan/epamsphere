package by.epam.sphere.storages;

import by.epam.sphere.entities.Point;
import by.epam.sphere.entities.Sphere;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Storage for storaging spheres.
 */
public class SphereStorage implements Storage {
    /**
     * Instance of SphereStorage that exists in a single copy.
     */
    private static SphereStorage instance;

    /**
     * HashMap for storaging spheres.
     */
    private HashMap<String, Sphere> sphereStorage;

    /**
     * Constructor for storage.
     */
    public SphereStorage() {
        sphereStorage = new HashMap<>();
    }

    /**
     * @return storage.
     */
    public HashMap<String, Sphere> getSphereStorage() {
        return sphereStorage;
    }

    /**
     * sets new storage.
     *
     * @param sphereStorageNew - new storage
     */
    public void setSphereStorage(
            final HashMap<String, Sphere> sphereStorageNew) {
        this.sphereStorage = sphereStorageNew;
    }

    /**
     * adds sphere to storage.
     *
     * @param sphere sphere to store
     */
    @Override
    public void add(final Object sphere) {
        Integer intDd = IDGenerator.generateNextId();
        String id = intDd + ((Sphere) sphere).getName();
        sphereStorage.put(id, (Sphere) sphere);
    }

    /**
     * removes sphere from storage.
     *
     * @param sphere - sphere to remove
     * @return true if removing was successfull
     */
    @Override
    public boolean remove(final Object sphere) {
        String id = "";
        if (sphereStorage.containsValue((Sphere) sphere)) {
            for (Map.Entry<String, Sphere> pair : sphereStorage.entrySet()) {
                if (pair.getValue().equals(sphere)) {
                    id = pair.getKey();
                    break;
                }
            }
            sphereStorage.remove(id, (Sphere) sphere);
            return true;
        }
        return false;
    }

    /**
     * modifies sphere in storage.
     *
     * @param id            - id of sphere
     * @param newParameters - new sphere's parameters
     * @return - true if corresponding sphere exists,
     * false if not
     */
    public boolean modify(final String id, final List<Object> newParameters) {
        Sphere sphere = sphereStorage.get(id);
        if (sphere != null) {
            sphere.setCenter((Point) newParameters.get(0));
            sphere.setRadius((double) newParameters.get(1));
            return true;
        }
        return false;
    }

    /**
     * String representation of SphereStorage class.
     *
     * @return String representation of SphereStorage class
     */
    @Override
    public String toString() {
        return sphereStorage.toString();
    }

    /**
     * @return instance of SphereStorage.
     */
    public static SphereStorage getInstance() {
        if (instance == null) {
            instance = new SphereStorage();
        }
        return instance;
    }
}


