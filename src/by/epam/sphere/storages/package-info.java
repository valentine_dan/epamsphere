/**
 * Package that contains classes for storaging spheres.
 *
 * SphereStorage - storage for spheres that encapsulates
 * HashMap for storaging. This class implements Singleton pattern
 */
package by.epam.sphere.storages;
