package by.epam.sphere.storages;

import by.epam.sphere.entities.GeometricFigure;

import java.util.List;

public interface Storage {
    void add(Object object);
    boolean remove(Object Object);
    boolean modify(String id, List<Object> newParameters);
}

