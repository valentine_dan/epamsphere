package by.epam.sphere.run;

import by.epam.sphere.creators.SphereCreator;
import by.epam.sphere.entities.Point;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.parsers.DataParser;
import by.epam.sphere.readers.FileReader;
import by.epam.sphere.repository.SphereRepository;
import by.epam.sphere.repository.queryspecification.findqueries.*;
import by.epam.sphere.repository.queryspecification.sortqueries.*;
import by.epam.sphere.storages.SphereStorage;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        SphereCreator sphereCreator = new SphereCreator();
        FileReader fileReader = new FileReader();
        DataParser stringParser = new DataParser();

        List<String> data = fileReader.readFile(new File("data/input.txt"));

        SphereRepository sr = new SphereRepository(SphereStorage.getInstance());
        for (String str:data) {
            List<Object> parsedData = stringParser.parseData(str);
            Sphere sphere = sphereCreator.createFigure(parsedData);
            if (sphere != null) {
                sr.add(sphere);
            }
        }

        /*System.out.println(sr.query(new FindBySurfaceAreaQuery(1000, 10000)));
        System.out.println(sr.query(new SortByRadiusQuery()));
        System.out.println(sr.query(new FindByCenterQuery(new Point(0,1,0))));
        System.out.println(sr.query(new FindByVolumeQuery(7200, 7800)));
        System.out.println(sr.query(new FindByDistanceFromAxesOriginQuery(10, 35)));
        System.out.println(sr.query(new FindByRadiusQuery(12)));
        System.out.println(sr.query(new FindByNameQuery("sec")));*/
        System.out.println(sr.query(new SortByNameQuery()));
        /*System.out.println(sr.query(new SortBySurfaceAreaQuery()));
        System.out.println(sr.query(new SortByVolumeQuery()));
        System.out.println(sr.query(new SortBySurfaceAreaQuery()));
        System.out.println(sr.query(new SortByXCoordinateQuery()));
        System.out.println(sr.query(new SortByYCoordinateQuery()));
        System.out.println(sr.query(new SortByZCoordinateQuery()));
        sr.modify("2sec", List.of(new Point(0, 12, 3), 24.0));
        System.out.println(SphereStorage.getInstance().getSphereStorage());
        System.out.println(sr.query(new FindByIdQuery("2sec")));*/
    }
}
