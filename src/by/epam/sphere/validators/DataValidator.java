package by.epam.sphere.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Class-validator for check whether input data has
 * correct data-type or not.
 *
 * @author Valentin Danilov
 */
public final class DataValidator {
    private static final Logger LOG = LogManager.getLogger();

    /**
     * Default constructor that prevents
     * from creating instance of this class.
     */
    private DataValidator() {
    }

    /**
     * Method for checking if data has
     * correct data-type or not.
     *
     * @param data - parsed data
     * @return true if data is OK, else returns false
     */
    public static boolean isValid(final List<Object> data) {
        final int radiusIndex = 4;
        if (isSphere(data)) {
            return true;
        } else {
            LOG.error("Can't create sphere with such radius: "
                    + data.get(radiusIndex));
            return false;
        }
    }

    /**
     * Method that checks if current object is sphere.
     * @param data - list of parameters to create sphere
     * @return true, if radius > 0
     */
    public static boolean isSphere(final List<Object> data) {
        final int radiusIndex = 4;
        return Double.compare((Double) data.get(radiusIndex), 0) == 1;
    }
}
