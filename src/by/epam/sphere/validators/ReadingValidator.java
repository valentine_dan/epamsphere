package by.epam.sphere.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;


/**
 * Class for checking if string has valid
 * number of arguments or not.
 *
 * @author Valentin Danilov
 */
public final class ReadingValidator {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * private constructor that prevents
     * from creating instance of this class.
     */
    private ReadingValidator() {
    }

    /**
     * Method checks if file exist or not.
     *
     * @param file - file containing data
     * @return true if valid, else returns false
     */
    public static boolean isValid(final File file) {
        boolean isEmpty = isEmpty(file);
        if (file.exists() && !isEmpty) {
            return true;
        } else {
            LOG.error("Problems with file");
            return false;
        }
    }

    /**
     * Auxiliary method that checks if file is empty.
     *
     * @param file - file to check
     * @return false is file is not empty, else false
     */
    private static boolean isEmpty(final File file) {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file)));
            String infoLine;
            String dataLine;
            infoLine = br.readLine();
            if (infoLine != null) {
                dataLine = br.readLine();
                if (dataLine != null) {
                    LOG.info("File is not empty!");
                } else {
                    LOG.error("There is no data to read!");
                    return true;
                }
            } else {
                LOG.error("File is empty!");
                return true;
            }
        } catch (IOException e) {
            LOG.error("Stream was not closed correctly");
            LOG.error(e.getStackTrace());
        }
        return false;
    }

}
