/**
 * Package for classes that validate input data.
 *
 * StringDataValidator - validator that checks if input data can
 * be parsed to double
 *
 * DataValidator - validator for data already parsed to double
 * Checks if object based on this data can be created
 *
 * ReadingValidator - validator for file, that contains input data
 * Checks if file exists and is not empty
 */
package by.epam.sphere.validators;
