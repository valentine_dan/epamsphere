package by.epam.sphere.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator class for validating string data.
 */
public final class StringDataValidator {
    /**
     * Regular expression for checking
     * if string can be parsed to double.
     */
    private static final Pattern DOUBLE_PATTERN =
            Pattern.compile("[+-]?\\d+[.]?\\d*");
    private static final Logger LOG = LogManager.getLogger();

    /**
     * private constructor that prevents
     * from creating instance of this class.
     */
    private StringDataValidator() {
    }

    /**
     * Method that checks if list of parameters is valid.
     *
     * @param data - input data
     * @return true if valid, else false
     */
    public static boolean isValid(final List<String> data) {
        boolean isValid;
        final int numberOfParams = 5;
        if (data.size() == numberOfParams) {
            if (data.stream().skip(1).allMatch(str -> {
                Matcher matcher = DOUBLE_PATTERN.matcher(str);
                return matcher.matches();
            })) {
                isValid = true;
            } else {
                LOG.error("Data " + data + " cannot be parsed to double");
                isValid = false;
            }
        } else {
            LOG.error("Illegal number of parameters: " + data);
            isValid = false;
        }
        return isValid;
    }
}

