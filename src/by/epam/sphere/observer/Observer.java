package by.epam.sphere.observer;

/**
 * Interface that will implement Observer pattern.
 * Classes that implements this interface
 * will be entities that can observe
 */
public interface Observer {
    /**
     * Updates current values in observer.
     * @param newValue - values according to
     *                 which data will be updated
     */
    void update(double newValue);
}

