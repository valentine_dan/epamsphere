package by.epam.sphere.observer;

/**
 * Interface that will implement Observer pattern.
 * Classes that implements this interface
 * will be entities that can be observed
 */

public interface Observable {
    /**
     * Notify subscribers that state was changed.
     * @param newRad - changed radius
     */
    void notify(double newRad);
}
