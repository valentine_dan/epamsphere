/**
 * Package that contains action classes which
 * work with entity-classes and calculate different
 * characteristics of entity-classes.
 *
 * SphereAction - class that contains method for calculating
 * characteristics of Spheres
 */
package by.epam.sphere.actions;
