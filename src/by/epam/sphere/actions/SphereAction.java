package by.epam.sphere.actions;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.enumerations.CoordPlanes;

/**
 * This class is created to makeQuery
 * characteristics of sphere.
 *
 * @author Valentin Danilov
 */
public final class SphereAction {
    /**
     *
     */
    private Sphere currentSphere;

    /**
     * Constructor to create instance of this class
     * based on sphere.
     *
     * @param newSphere - basis sphere
     */
    public SphereAction(final Sphere newSphere) {
        currentSphere = newSphere;
    }

    /**
     * Method that calculates surface area of Sphere.
     *
     * @return calculated area
     */
    public static double calcSurfaceArea(double radius) {
        final int four = 4;
        return four * Math.PI * Math.pow(radius, 2);
    }

    /**
     * Method that calculates volume of Sphere.
     *
     * @return calculated volume
     */
    public static  double calcVolume(double radius) {
        final int four = 4;
        final int three = 3;
        return four * Math.PI
                * Math.pow(radius, three) / three;
    }

    /**
     * Auxiliary method that calculates ratio between
     * volumes of two parts of Sphere divided by plane.
     * As plane that divides Sphere is parallel to one of the coordinate
     * planes we need only coordinate of intersection
     * of dividing plane and corresponding axis and name of coordinate plane
     * to calculate ratio.
     *
     * @param centerCoord   - absolute value of corresponding coordinate
     * @param divPlaneCoord - coordinate of intersection of dividing plane
     *                      corresponding axis.
     * @return ratio
     */
    private double calcRatio(final double divPlaneCoord,
                             final double centerCoord) {
        double fullVolume = calcVolume(currentSphere.getRadius());
        double ratio = 0;
        if (divPlaneCoord < (centerCoord - currentSphere.getRadius())
                || divPlaneCoord > (centerCoord
                + currentSphere.getRadius())) {
            return -1;
        }
        double h = currentSphere.getRadius()
                - Math.abs(Math.abs(centerCoord) - divPlaneCoord);
        double minVol = calcSegmentVolume(h);
        ratio = minVol / (fullVolume - minVol);
        return ratio;
    }

    /**
     * Method that calculates volume of sphere's segment.
     *
     * @param height - hight of dividing plane
     * @return corresponding volume
     */
    public double calcSegmentVolume(final double height) {
        final int two = 2;
        final int three = 3;
        return two * Math.PI
                * Math.pow(currentSphere.getRadius(), 2) * height / three;
    }

    /**
     * Mehthod that finds ratio between parts of sphere.
     *
     * @param planeCoord    - hight of dividing plane
     * @param parallelPlane - coordinate plane which is parallel to
     *                      dividing plane
     * @return corresponding ratio
     */
    public double findVolumeRatio(final double planeCoord,
                                  final CoordPlanes parallelPlane) {
        double x = currentSphere.getCenter().getX();
        double y = currentSphere.getCenter().getY();
        double z = currentSphere.getCenter().getZ();
        double ratio = 0;
        switch (parallelPlane) {
            case XY:
                ratio = calcRatio(planeCoord, z);
                break;
            case XZ:
                ratio = calcRatio(planeCoord, y);
                break;
            case YZ:
                ratio = calcRatio(planeCoord, x);
                break;
            default:
                ratio = -1;
                break;
        }
        return ratio;
    }

    /**
     * Method that identifies if sphere touches coordinate plane
     * or not.
     *
     * @return true if touches, else return false
     */
    public boolean doesTouchCoordPlane() {
        double absX = Math.abs(currentSphere.getCenter().getX());
        double absY = Math.abs(currentSphere.getCenter().getY());
        double absZ = Math.abs(currentSphere.getCenter().getZ());
        double r = currentSphere.getRadius();

        return absX - r == 0
                || absY - r == 0
                || absZ - r == 0;
    }
}
