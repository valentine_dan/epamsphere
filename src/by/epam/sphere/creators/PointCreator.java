package by.epam.sphere.creators;

import by.epam.sphere.entities.Point;

import java.util.List;

/**
 * Class that produces point objects.
 *
 * @author Valentin Danilov
 */
public class PointCreator implements GeometricFigureCreator<Point> {
    /**
     * Method that constructs point based on input parameters.
     * @param params - parameters for creating point
     * @return constructed object
     */
    public Point createFigure(final List<Object> params) {
        final int xIndex = 1;
        final int yIndex = 2;
        final int zIndex = 3;
        double x = (double) params.get(xIndex);
        double y = (double) params.get(yIndex);
        double z = (double) params.get(zIndex);
        return new Point(x, y, z);
    }
}

