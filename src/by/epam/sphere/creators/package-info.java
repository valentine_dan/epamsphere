/**
 *Package contains classes which helps to create
 * instances of entity-classes.
 *
 * GeometricFigureCreator - interface for creators
 *
 * PointCreator - constructs instances of point
 *
 * SphereCreator - constructs instances of sphere
 */
package by.epam.sphere.creators;
