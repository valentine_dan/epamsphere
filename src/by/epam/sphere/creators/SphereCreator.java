package by.epam.sphere.creators;

import by.epam.sphere.entities.Point;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.validators.DataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Class that contains methods which create instances of
 * Sphere class.
 *
 * @author Valentin Danilov
 */
public class SphereCreator implements GeometricFigureCreator<Sphere> {
    private static final Logger LOG = LogManager.getLogger();

    /**
     * Method that constructs sphere object based on input data.
     *
     * @param data - input data
     * @return created object if input data is correct, else
     * returns null
     */
    @Override
    public Sphere createFigure(final List<Object> data) {
        PointCreator pointCreator = new PointCreator();
        final int radiusIndex = 4;
        final int nameIndex = 0;
        if (DataValidator.isValid(data)) {
            Point center = pointCreator.createFigure(data);
            double radius = (double) data.get(radiusIndex);
            String name = (String) data.get(nameIndex);
            return new Sphere(name, center, radius);
        } else {
            LOG.debug("Can't create Sphere with this parameters: " + data);
            return null;
        }
    }
}

