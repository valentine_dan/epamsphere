package by.epam.sphere.creators;

import by.epam.sphere.entities.GeometricFigure;

import java.util.List;

/**
 * Interface for classes, that create instances of GeometricFigure classes
 *
 * @param <T> - identifies what figure will create class,
 *           that implements this interface
 */
interface GeometricFigureCreator<T extends GeometricFigure> {
    T createFigure(List<Object> params);
}
