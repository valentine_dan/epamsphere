package by.epam.sphere.repository;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.repository.queryspecification.Query;
import by.epam.sphere.storages.SphereStorage;
import by.epam.sphere.storages.Storage;

import java.util.List;

public class SphereRepository {
    private Storage storage;

    public SphereRepository(Storage newStorage){
        storage = newStorage;
    }

    public void add(Sphere sphere){
        storage.add(sphere);
    }

    public boolean remove(Sphere sphere){
        return storage.remove(sphere);
    }

    public boolean modify(String id, List<Object> newParameters){
        return storage.modify(id, newParameters);
    }

    public List<Sphere> query(Query query){
        return query.makeQuery();
    }

    @Override
    public String toString() {
        return "SphereRepository{" +
                "storage=" + storage +
                '}';
    }
}
