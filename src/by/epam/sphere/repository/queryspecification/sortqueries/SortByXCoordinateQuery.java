package by.epam.sphere.repository.queryspecification.sortqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class for query that sorts spheres by it's center's X-coordinate.
 */
public class SortByXCoordinateQuery implements SortQuery {
    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        List<Sphere> spheres = new
                ArrayList<>(
                SphereStorage.
                        getInstance().
                        getSphereStorage().
                        values());

        spheres.sort(Comparator.comparingDouble(
                sphere -> sphere.getCenter().getX()));
        return spheres;
    }
}
