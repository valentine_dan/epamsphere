package by.epam.sphere.repository.queryspecification.sortqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.repository.queryspecification.Query;

import java.util.List;

/**
 * Interface for sort queries
 */
public interface SortQuery extends Query {
    List<Sphere> makeQuery();
}
