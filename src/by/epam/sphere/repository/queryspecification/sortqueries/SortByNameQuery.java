package by.epam.sphere.repository.queryspecification.sortqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class for query that sorts sphere by name.
 */
public class SortByNameQuery implements SortQuery {
    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        List<Sphere> spheres = new ArrayList<>(
                SphereStorage.getInstance().
                        getSphereStorage().
                        values()
        );
        spheres.sort(Comparator.comparing(Sphere::getName));
        return spheres;
    }
}
