package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for query that finds spheres by radius.
 */
public class FindByRadiusQuery implements FindQuery {
    /**
     * Sphere's radius.
     */
    private double radius;

    /**
     * Constructor for query.
     *
     * @param radiusNew - radius
     */
    public FindByRadiusQuery(final double radiusNew) {
        this.radius = radiusNew;
    }

    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        List<Sphere> spheres = new ArrayList<>(
                sphereStorage.getSphereStorage().values());
        return spheres.stream().filter(sphere ->
                Double.compare(sphere.getRadius(), radius) == 0).
                collect(Collectors.toList());
    }
}
