package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for query that finds spheres by surface area.
 */
public class FindBySurfaceAreaQuery implements FindQuery {
    /**
     * lower bound for area.
     */
    private double lowerBound;
    /**
     * higher bound for area.
     */
    private double higherBound;

    /**
     * Constructor for query.
     *
     * @param lowerBoundNew  - lower bound
     * @param higherBoundNew higher bound
     */
    public FindBySurfaceAreaQuery(final double lowerBoundNew,
                                  final double higherBoundNew) {
        this.lowerBound = lowerBoundNew;
        this.higherBound = higherBoundNew;
    }

    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        List<Sphere> spheres = new ArrayList<>(
                sphereStorage.getSphereStorage().values());
        List<Sphere> resultList = new ArrayList<>();
        for (Sphere sphere
                : spheres) {
            double currentArea = sphere.getMeasures().getSphereSurfaceArea();
            if (currentArea <= higherBound && currentArea >= lowerBound) {
                resultList.add(sphere);
            }
        }
        return resultList;
    }
}
