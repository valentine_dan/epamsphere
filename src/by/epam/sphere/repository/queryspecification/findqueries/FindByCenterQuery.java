package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Point;
import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Query class for finding sphere with defined center.
 */
public class FindByCenterQuery implements FindQuery {
    /**
     * Center of sphere.
     */
    private Point center;

    /**
     * Constructor for query.
     *
     * @param centerNew - new value for center
     */
    public FindByCenterQuery(final Point centerNew) {
        this.center = centerNew;
    }

    /**
     * Make corresponding query.
     *
     * @return list of spheres, that match query
     */
    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        List<Sphere> spheres = new ArrayList<>(
                sphereStorage.getSphereStorage().values());
        return spheres.stream().filter(
                sphere -> sphere.getCenter().equals(center)).
                collect(Collectors.toList());
    }
}
