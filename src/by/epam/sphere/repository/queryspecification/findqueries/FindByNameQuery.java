package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for query that finds spheres by name.
 */
public class FindByNameQuery implements FindQuery {
    /**
     * Sphere's name.
     */
    private String name;

    /**
     * Constructor for query.
     *
     * @param nameNew - name
     */
    public FindByNameQuery(final String nameNew) {
        this.name = nameNew;
    }

    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        List<Sphere> spheres = new ArrayList<>(
                sphereStorage.getSphereStorage().values());
        return spheres.stream().filter(sphere -> sphere.getName().equals(name)).
                collect(Collectors.toList());
    }
}
