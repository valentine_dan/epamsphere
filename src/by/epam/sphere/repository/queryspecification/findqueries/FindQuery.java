package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.repository.queryspecification.Query;

import java.util.List;

/**
 * Interface for Find queries.
 */
public interface FindQuery extends Query {
    /**
     * Makes specified query
     * @return list of spheres matching to query
     */
    List<Sphere> makeQuery();
}

