package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindByIdQuery implements FindQuery {
    private String id;

    public FindByIdQuery(String idNew) {
        this.id = idNew;
    }

    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        HashMap<String, Sphere> spheres = sphereStorage.getSphereStorage();
        List<Sphere> resultList = new ArrayList<>();

        for (Map.Entry<String, Sphere> pair
             :spheres.entrySet()) {
            if(pair.getKey().equals(id)){
                resultList.add(pair.getValue());
            }
        }
        return resultList;
    }
}
