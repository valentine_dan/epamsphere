package by.epam.sphere.repository.queryspecification.findqueries;

import by.epam.sphere.entities.Sphere;
import by.epam.sphere.storages.SphereStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for query that finds sphere by distance from axes origin.
 */
public class FindByDistanceFromAxesOriginQuery implements FindQuery {
    /**
     * Lower bound of distance.
     */
    private double lowerBound;
    /**
     * Higher bound of distance.
     */
    private double higherBound;

    /**
     * Constructor for query.
     *
     * @param lowerBoundNew  - lower bound of sphere's
     *                       distance from axes origin
     * @param higherBoundNew - higher bound of sphere's
     *                       distance from axes origin
     */
    public FindByDistanceFromAxesOriginQuery(final double lowerBoundNew,
                                             final double higherBoundNew) {
        this.lowerBound = lowerBoundNew;
        this.higherBound = higherBoundNew;
    }

    /**
     * Makes specified query.
     *
     * @return list of spheres matching to query
     */
    @Override
    public List<Sphere> makeQuery() {
        SphereStorage sphereStorage = SphereStorage.getInstance();
        List<Sphere> spheres = new ArrayList<>(
                sphereStorage.getSphereStorage().values());
        return spheres.stream().filter(sphere -> {
            double currentDistance = Math.sqrt(
                    Math.pow(sphere.getCenter().getX(), 2)
                            + Math.pow(sphere.getCenter().getY(), 2)
                            + Math.pow(sphere.getCenter().getZ(), 2));
            return currentDistance >= lowerBound
                    && currentDistance <= higherBound;
        }).collect(Collectors.toList());
    }
}
