package by.epam.sphere.repository.queryspecification;

import by.epam.sphere.entities.GeometricFigure;
import by.epam.sphere.entities.Sphere;

import java.util.List;


/**
 * Base interface for query classes
 */
public interface Query {
    /**
     * Method for making query
     * @return List of specified data
     */
    List<Sphere> makeQuery();
}
